// Should return a string representing the ranges
function toRange(arr) {
    return [...new Set(arr)].sort((a, b) => a - b).reduce((pV, cV, cI) => {
        let i = -1;
        if ((i = pV.findIndex(e => e.endsWith('_' + (cV - 1)))) !== -1) {
            let range = pV.splice(i, 1)[0].split("_");
            pV.push(range[0] + '_' + cV.toString());
        } else if ((i = pV.findIndex(e => e === (cV - 1).toString())) !== -1) {
            let num = pV.splice(i, 1)[0];
            pV.push(num + '_' + cV.toString());
        } else {
            pV.push(cV.toString());
        }
        return pV;
    }, []).toString();
}

// Should return an array
function toArray(str) {
    return str === '' ? [] : str.split(",").map(e => e.split('_')).map(e => e.map(s => parseInt(s))).reduce((pV, cV, cI) => {
        if (cV.length > 1)
            for (let n = cV[0]; n <= cV[1]; n++) pV.push(n);
        else
            pV.push(cV[0]);
        return pV;
    }, []);
}
